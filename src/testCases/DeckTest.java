package testCases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import poker.Deck;

public class DeckTest
{
	
	Deck deck = new Deck();

	@Test
	public void testConstructDeck()
	{
		assertEquals(false, deck.constructDeck());
	}

	@Test
	public void testDraw()
	{
		assertEquals(null, deck.draw());
	}

	@Test
	public void testShuffle()
	{
		assertEquals(false, deck.shuffle());
	}

	@Test
	public void testToString()
	{
		assertEquals(null, deck.toString());
	}

	@Test
	public void testClone()
	{
		assertEquals(null, deck.clone());
	}

}
