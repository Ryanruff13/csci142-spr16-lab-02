package testCases;

import static org.junit.Assert.*;
import org.junit.Test;
import playerCode.Player;


public class PlayerTest
{
	
	Player player = new Player(null);
	
	@Test
	public void testValidateName()
	{
		assertEquals(null, player.validateName(""));
	}
	
	@Test
	public void testIncrementWins()
	{
		assertEquals(0, player.incrementWins());
	}
	
	@Test
	public void testToString()
	{
		assertEquals(null, player.toString());
	}
	
	@Test
	public void testClone()
	{
		assertEquals(null, player.clone());
	}
	
	@Test
	public void testGetHand()
	{
		assertEquals(null, player.getHand());
	}
	
	public void testGetName()
	{
		assertEquals(null, player.getName());
	}
	
	public void testGetNumberWins()
	{
		assertEquals(null, player.getNumberWins());
	}
	
	public void testGetAmAI()
	{
		assertEquals(null, player.getAmAI());
	}

}
