package testCases;
import static org.junit.Assert.*;
import org.junit.Test;
import poker.Hand;

public class HandTest
{
	
	Hand hand = new Hand((short) 0);

	@Test
	public void testAdd()
	{
		assertEquals(false, hand.add());
	}

	@Test
	public void testDiscard()
	{
		assertEquals(false, hand.discard((short) 0));
	}

	@Test
	public void testToString()
	{
		assertEquals(null, hand.toString());
	}
	
	/*public void testOrderCards()
	{
		
	}*/

	@Test
	public void getNumberCardsInHand()
	{
		assertEquals(null, hand.getNumberCardsInHand());
	}

}
