package testCases;

import static org.junit.Assert.*;
import org.junit.Test;

import playerCode.Player;
import poker.PokerModel;

public class PokerModelTest
{
	
	PokerModel pokerModel = new PokerModel(null);

	@Test
	public void testSwitchTurns()
	{
		assertEquals(0, pokerModel.switchTurns());
	}

	@Test
	public void testDealCards()
	{
		assertEquals(false, pokerModel.dealCards());
	}

	@Test
	public void testDetermineWinner()
	{
		assertEquals(null, pokerModel.determineWinner());
	}

	@Test
	public void testResetGame()
	{
		assertEquals(false, pokerModel.resetGame());
	}

	@Test
	public void testGetPlayerUp()
	{
		assertEquals(null, pokerModel.getPlayerUp());
	}

	@Test
	public void testGetPlayer()
	{
		assertEquals(null, pokerModel.getPlayer(3));
	}

	@Test
	public void testGetNumberDraws()
	{
		assertEquals(0, pokerModel.getNumberDraws());
	}

	@Test
	public void testGetIndexPlayerUp()
	{
		assertEquals(0, pokerModel.getIndexPlayerUp());
	}	
	
}
