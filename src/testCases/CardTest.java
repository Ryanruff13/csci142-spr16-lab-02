package testCases;

import static org.junit.Assert.*;
import org.junit.Test;
import poker.Card;

public class CardTest
{

	Card card = new Card(null, null, null);
	
	@Test
	public void testIsFaceUp()
	{
		assertEquals(false, card.isFaceUp());
	}

	@Test
	public void testFlip()
	{
		assertEquals(true, true);
	}

	@Test
	public void testGetType()
	{
		assertEquals(0, card.getType());
	}

	@Test
	public void testGetSuit()
	{
		assertEquals(null, card.getSuit());
	}

	@Test
	public void testGetImage()
	{
		assertEquals(null, card.getImage());
	}

	@Test
	public void testToString()
	{
		assertEquals(true, true);
	}

	@Test
	public void testClone()
	{
		assertEquals(true, true);
	}

}
