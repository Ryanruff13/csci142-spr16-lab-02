package testCases;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import poker.PokerHand;

public class PokerHandTest
{
	
	PokerHand pokerHand = new PokerHand((short) 0);
	
	@Test
	public void testDetermineRanking()
	{
		assertEquals(0, pokerHand.determineRanking());
	}

	@Test
	public void testCompareTo()
	{
		PokerHand pokerHand2 = new PokerHand((short) 0);
		assertEquals(0, pokerHand.compareTo(pokerHand2));
	}

	@Test
	public void testToString()
	{
		assertEquals(null, pokerHand.toString());
	}

	@Test
	public void testGetNumberCards()
	{
		assertEquals(null, pokerHand.getNumberCards());
	}

	@Test
	public void testGetMaxNumberCards()
	{
		assertEquals((short) 0, pokerHand.getMaxNumberCards());
	}

	@Test
	public void testIsHighCard()
	{
		assertEquals(false, pokerHand.isHighCard());
	}

	@Test
	public void testIsPair()
	{
		assertEquals(false, pokerHand.isPair());
	}

	@Test
	public void testIsTwoPair()
	{
		assertEquals(false, pokerHand.isTwoPair());
	}

	@Test
	public void testIsFlush()
	{
		assertEquals(false, pokerHand.isFlush());
	}

	@Test
	public void testIsFullHouse()
	{
		assertEquals(false, pokerHand.isFullHouse());
	}

	@Test
	public void testIsFourOfAKind()
	{
		assertEquals(false, pokerHand.isFourOfAKind());
	}

	@Test
	public void testIsStraightFlush()
	{
		assertEquals(false, pokerHand.isStraightFlush());
	}

	@Test
	public void testIsRoyalFlush()
	{
		assertEquals(false, pokerHand.isRoyalFlush());
	}

}
