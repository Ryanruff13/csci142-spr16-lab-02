package testCases;

import static org.junit.Assert.*;
import org.junit.Test;
import poker.CardType;

public class CardTypeTest
{
	
	CardType cardType = new CardType((short) 0);
	
	@Test
	public void testGetType()
	{
		assertEquals(0, cardType.getType());
	}

}
