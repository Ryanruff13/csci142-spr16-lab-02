package playerCode;

import poker.Hand;

public class Player
{

	String DEFAULT_NAME = null;
	String myName;
	short myNumberOfWins;
	boolean myAmAI;
	Hand myHand;
	
	public Player(String name)
	{
		/*if (name == null)
		{
			name = DEFAULT_NAME;
		}*/
	}
	
	public boolean validateName(String name)
	{
		/*boolean output = true;

		for (short i = 0; i < 0 name.length; i++)
		{
			if (!(name.charAt(i) >= '0' && name.charAt(i) <= 9))
			{
				output = false;
				break;
			}
		}
		return output;*/
		return false;
		
	}
	
	public int incrementWins()
	{
		/*myNumberOfWins++;
		return myNumberOfWins;*/
		return 0;
	}
	
	public String toString()
	{
		return "Name: " + myName + ", Wins: " + myNumberOfWins;
	}
	
	public Object clone()
	{
		return null;
	}
	
	public Hand getHand()
	{
		return myHand;
	}
	
	public String getName()
	{
		return myName;
	}
	
	public short getNumberWins()
	{
		return myNumberOfWins;
	}
	
	public boolean getAmAI()
	{
		return myAmAI;
	}

}
