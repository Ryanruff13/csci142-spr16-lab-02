package poker;

public class PokerHand
{

	short myNumberCards;
	short myMaxNumberCards;
	
	public PokerHand(short maxNum)
	{
		super();
		myNumberCards = maxNum;
	}
	
	public short determineRanking()
	{
		return 0;
	}
	
	//public PokerHandRanking()
	{
		
	}
	
	public int compareTo(PokerHand pokerHand)
	{
		return 0;
	}
	
	public String toString()
	{
		return null;
	}
	
	//public PokerHandRanking getRanking()
	{
		//return null;
	}
	
	public int getNumberCards()
	{
		return myNumberCards;	
	}
	
	public int getMaxNumberCards()
	{
		return myMaxNumberCards;
	}
	
	public boolean isHighCard()
	{
		return false;
	}
	
	public boolean isPair()
	{
		return false;
	}
	
	public boolean isTwoPair()
	{
		return false;
	}
	
	public boolean isFlush()
	{
		return false;
	}
	
	public boolean isFullHouse()
	{
		return false;
	}
	
	public boolean isFourOfAKind()
	{
		return false;
	}
	
	public boolean isStraightFlush()
	{
		return false;
	}
	
	public boolean isRoyalFlush()
	{
		return false;
	}

}
